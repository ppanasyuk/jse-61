package ru.t1.panasyuk.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.model.Project;

import java.util.List;

@Repository
@Scope("prototype")
public interface ProjectRepository extends AbstractUserOwnedRepository<Project> {

    @Nullable
    @Query("FROM Project m WHERE m.user.id = :userId ORDER BY m.created DESC")
    List<Project> findByIndex(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @NotNull
    @Query("FROM Project m WHERE m.user.id = :userId")
    List<Project> findByUserId(@NotNull @Param("userId") String userId, @NotNull Sort sort);

    @Modifying
    @Query("DELETE FROM Project m WHERE m.user.id = :userId")
    void deleteByUserId(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Project m WHERE m.user.id = :userId ORDER BY m.created DESC")
    List<Project> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Project m WHERE m.user.id = :userId ORDER BY m.name")
    List<Project> findAllOrderByName(@NotNull @Param("userId") String userId);

    @NotNull
    @Query("FROM Project m WHERE m.user.id = :userId ORDER BY m.status")
    List<Project> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Nullable
    @Query("FROM Project m WHERE m.user.id = :userId AND m.id = :id ORDER BY m.created DESC")
    Project findById(@NotNull @Param("userId") String userId, @NotNull @Param("id") String id);

    @Query("SELECT COUNT(m) FROM Project m WHERE m.user.id = :userId")
    long countByUserId(@NotNull @Param("userId") String userId);

}