package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.ProjectDTO;
import ru.t1.panasyuk.tm.dto.model.TaskDTO;

public interface IProjectTaskDtoService {

    @NotNull
    TaskDTO bindTaskToProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    ProjectDTO removeProjectById(@NotNull String userId, @Nullable String projectId);

    ProjectDTO removeProjectByIndex(@NotNull String userId, @Nullable Integer index);

    void clearProjects(@NotNull String userId);

    @NotNull
    TaskDTO unbindTaskFromProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

}