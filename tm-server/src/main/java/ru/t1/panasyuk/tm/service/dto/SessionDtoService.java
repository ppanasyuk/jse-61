package ru.t1.panasyuk.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.panasyuk.tm.api.service.dto.ISessionDtoService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.repository.dto.SessionDtoRepository;

import java.util.List;

@Service
public final class SessionDtoService extends AbstractUserOwnedDtoService<SessionDTO, SessionDtoRepository>
        implements ISessionDtoService {

    @Override
    @Transactional
    public void clear(@NotNull final String userId) {
        @NotNull final SessionDtoRepository repository = getRepository();
        repository.deleteByUserId(userId);
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        @NotNull final SessionDtoRepository repository = getRepository();
        result = repository.findFirstByUserIdAndId(userId, id) != null;
        return result;
    }

    @Override
    public long getSize(@NotNull final String userId) {
        long result;
        @NotNull final SessionDtoRepository repository = getRepository();
        result = repository.countByUserId(userId);
        return result;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll() {
        @Nullable final List<SessionDTO> sessions;
        @NotNull final SessionDtoRepository repository = getRepository();
        sessions = repository.findAll();
        return sessions;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@NotNull final String userId) {
        @Nullable final List<SessionDTO> models;
        @NotNull final SessionDtoRepository repository = getRepository();
        @NotNull final Sort sort = Sort.by(Sort.Direction.DESC, "created");
        models = repository.findByUserId(userId, sort);
        return models;
    }

    @Nullable
    @Override
    public SessionDTO findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable SessionDTO model;
        @NotNull final SessionDtoRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        model = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElse(null);
        return model;
    }

    @Nullable
    @Override
    public SessionDTO findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final SessionDTO model;
        @NotNull final SessionDtoRepository repository = getRepository();
        model = repository.findFirstByUserIdAndId(userId, id);
        return model;
    }

    @NotNull
    @Override
    @Transactional
    public SessionDTO remove(@Nullable final SessionDTO session) {
        if (session == null) throw new EntityNotFoundException();
        @NotNull final SessionDtoRepository repository = getRepository();
        repository.delete(session);
        return session;
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final SessionDTO removedModel;
        @NotNull final SessionDtoRepository repository = getRepository();
        @NotNull final Pageable pageable = PageRequest.of(index - 1, 1);
        removedModel = repository
                .findByIndex(userId, pageable)
                .stream()
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
        repository.delete(removedModel);
        return removedModel;
    }

    @Nullable
    @Override
    @Transactional
    public SessionDTO removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final SessionDTO removedModel;
        @NotNull final SessionDtoRepository repository = getRepository();
        removedModel = repository.findFirstByUserIdAndId(userId, id);
        if (removedModel == null) throw new EntityNotFoundException();
        repository.delete(removedModel);
        return removedModel;
    }

}