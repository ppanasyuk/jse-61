package ru.t1.panasyuk.tm.repository.dto;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.panasyuk.tm.dto.model.AbstractUserOwnedModelDTO;

@Repository
@Scope("prototype")
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDTO> extends AbstractDtoRepository<M> {
}