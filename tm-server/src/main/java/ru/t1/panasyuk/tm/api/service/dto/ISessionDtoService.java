package ru.t1.panasyuk.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.dto.model.SessionDTO;

import java.util.List;

public interface ISessionDtoService extends IUserOwnedDtoService<SessionDTO> {

    SessionDTO remove(@Nullable SessionDTO session);

    List<SessionDTO> findAll();

}
