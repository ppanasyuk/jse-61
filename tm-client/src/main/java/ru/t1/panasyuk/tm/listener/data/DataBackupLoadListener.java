package ru.t1.panasyuk.tm.listener.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.panasyuk.tm.dto.request.data.DataBackupLoadRequest;
import ru.t1.panasyuk.tm.event.ConsoleEvent;

@Component
public final class DataBackupLoadListener extends AbstractDataListener {

    @NotNull
    private static final String DESCRIPTION = "Load data from backup file.";

    @NotNull
    public static final String NAME = "data-backup-load";

    @Override
    @SneakyThrows
    @EventListener(condition = "@dataBackupLoadListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final DataBackupLoadRequest request = new DataBackupLoadRequest(getToken());
        domainEndpoint.loadDataBackup(request);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}