package ru.t1.panasyuk.tm.exception.system;

import org.jetbrains.annotations.NotNull;

public final class RequestNotSupportedException extends AbstractSystemException {

    public RequestNotSupportedException() {
        super("Error! Request not supported...");
    }

    public RequestNotSupportedException(@NotNull final String request) {
        super("Error! request \"" + request + "\" not supported");
    }

}